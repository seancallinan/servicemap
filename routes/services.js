const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

/* GET users listing. */
router.get('/:id', (req, res) => {

    fetch('https://www.metlink.org.nz/api/v1/ServiceLocation/' + req.params.id)
        .then(resp => resp.json())
        .then(json =>
        {
            const result = {
                type: 'FeatureCollection',
                features: json.Services.map(service => {
                    return {
                        type: 'Feature',
                        properties: {
                            direction: service.Direction,
                            originStopId: service.OriginStopID,
                            originStopName: service.OriginStopName,
                            behindSchedule: service.BehindSchedule? 'yes':'no',
                            delaySeconds: service.DelaySeconds,
                            destinationStopId: service.DestinationStopID,
                            destinationStopName: service.DestinationStopName,
                            vehicleRef: service.VehicleRef,
                            serviceName: service.Service.Name,
                            serviceCode: service.Service.Code
                        },
                        geometry: {
                            type: 'Point',
                            coordinates: [
                                parseFloat(service.Long),
                                parseFloat(service.Lat)
                            ]
                        }
                    };
                })
            };

            return res.send(result);
        })
        .catch(err => res.send(err));

});

module.exports = router;
